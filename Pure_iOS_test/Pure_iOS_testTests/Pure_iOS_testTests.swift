//
//  Pure_iOS_testTests.swift
//  Pure_iOS_testTests
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import XCTest
import TwitterKit
@testable import Pure_iOS_test

class Pure_iOS_testTests: XCTestCase {
    
  override func setUp() {
    super.setUp()
  }
    
  override func tearDown() {
    super.tearDown()
  }
  
  func testMainCellViewModelType() {
    let owner = Owner(id: 0, avatarUrl: "")
    let item = Item(id: 0, name: "", description: "", url: "", owner: owner)
    XCTAssertTrue(item.type == .github)
    
    guard let tweet = TWTRTweet(jsonDictionary: [AnyHashable : Any]()) else { return }
    XCTAssertTrue(tweet.type == .twitter)
  }
  
  func testAPI() {
    let exp = expectation(description: "retrieve github models")
    ApiClient.shared().request(apiMethod: "", responseClass: GithubResponse.self) { result in
      switch result {
      case .success:
        exp.fulfill()
      case .failure:
        XCTFail("Request shouldn't fail")
        exp.fulfill()
      }
    }
    
    waitForExpectations(timeout: 30.0, handler: nil)
  }
}
