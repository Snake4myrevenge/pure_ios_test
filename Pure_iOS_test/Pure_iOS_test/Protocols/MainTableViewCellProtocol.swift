//
//  MainTableViewCellProtocol.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/22/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

protocol MainTableViewCellProtocol: class {
  
  var titleLabel: UILabel! { get }
  var descriptionLabel: UILabel! { get }
  var avatarImageView: UIImageView! { get }
  
}

extension MainTableViewCellProtocol {
  
  func updateCellWithModel(_ model: MainTableViewCellViewModelProtocol) {
    titleLabel.text = model.title
    descriptionLabel.text = model.descriptionText
    
    avatarImageView.sd_addActivityIndicator()
    avatarImageView.sd_setIndicatorStyle(.whiteLarge)
    avatarImageView.sd_setImage(with: URL(string: model.avatarUrl)) { (_, _, _, _) in
      self.avatarImageView.sd_removeActivityIndicator()
    }
  }
  
}
