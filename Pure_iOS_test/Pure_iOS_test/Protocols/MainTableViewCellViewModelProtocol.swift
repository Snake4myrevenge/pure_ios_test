//
//  MainTableViewCellViewModelProtocol.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/22/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import TwitterKit

enum MainTableViewCellType {
  case none
  case twitter
  case github
}

protocol MainTableViewCellViewModelProtocol {
  
  var title: String { get }
  var descriptionText: String { get }
  var avatarUrl: String { get }
  
  var type: MainTableViewCellType { get }
}

extension MainTableViewCellViewModelProtocol {
  
  var type: MainTableViewCellType {
    if self is Item { return .github }
    if self is TWTRTweet { return .twitter }
    return .none
  }
  
}
