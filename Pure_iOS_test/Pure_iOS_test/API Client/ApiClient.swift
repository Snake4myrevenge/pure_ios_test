//
//  ApiClient.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import Alamofire
import NotificationBannerSwift

enum Result<T> {
  case success(T)
  case failure(Error)
}

enum Errors: Error {
  case badRequest
  case responseNotValidJson
  case noDataReturned
}

class ApiClient {
  
  // MARK: - Singleton.
  
  private static let sharedInstance = ApiClient()
  
  private init() { }
  
  class func shared() -> ApiClient { return self.sharedInstance }
  
  // MARK: - Public properties
  
  // MARK: - Private properties
  
  private let baseURL: String = "https://api.github.com/search/repositories?q=swift"
  
  // MARK: - Public API
  
  @discardableResult
  func request<T: Codable>(apiMethod: String,
                           method: HTTPMethod = .get,
                           parameters: Parameters? = nil,
                           encoding: ParameterEncoding = URLEncoding.default,
                           headers: HTTPHeaders? = nil,
                           responseClass: T.Type,
                           completion:@escaping (Result<T>) -> Void) -> DataRequest {
    return Alamofire.request(urlForApiMethod(apiMethod), method: method, parameters: parameters, encoding: encoding, headers: headers)
      .validate(statusCode: 200..<300)
      .responseJSON { response in
        switch response.result {
        case .success:
          guard let data = response.data else {
            self.showBanner(title: "", subtitle: "")
            completion(.failure(Errors.noDataReturned))
            return
          }
          let decoder = JSONDecoder()
          guard let responseObject = try? decoder.decode(T.self, from: data) else {
            self.showBanner(title: "", subtitle: "")
            completion(.failure(Errors.responseNotValidJson))
            return
          }
          completion(.success(responseObject))
        case .failure:
          self.showBanner(title: "Error", subtitle: "Please try again later")
          completion(.failure(Errors.badRequest))
        }
    }
  }
  
  // MARK: - Private API
  
  private func urlForApiMethod(_ apiMethod: String) -> String {
    return baseURL + apiMethod
  }
  
  private func showBanner(title: String, subtitle: String) {
    let banner = NotificationBanner(title: title, subtitle: subtitle, style: .warning)
    banner.show()
  }
  
}
