//
//  Owner.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation

struct Owner: Codable {
  
  let id: Int
  let avatarUrl: String
  
  enum CodingKeys : String, CodingKey {
    case id = "id"
    case avatarUrl = "avatar_url"
  }
}
