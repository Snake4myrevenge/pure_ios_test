//
//  Item.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation

struct Item: Codable {
  
  let id: Int
  let name: String
  let description: String
  let url: String
  let owner: Owner
  
  enum CodingKeys : String, CodingKey {
    case id = "id"
    case name = "name"
    case description = "description"
    case url = "url"
    case owner = "owner"
  }
  
}

extension Item: MainTableViewCellViewModelProtocol {
  
  var title: String { return name }
  var descriptionText: String { return description }
  var avatarUrl: String { return owner.avatarUrl }
  
}
