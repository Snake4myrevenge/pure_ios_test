//
//  GithubResponse.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation

struct GithubResponse: Codable {
  
  let totalCount: Int
  let items: [Item]
  
  enum CodingKeys : String, CodingKey {
    case totalCount = "total_count"
    case items = "items"
  }
  
}
