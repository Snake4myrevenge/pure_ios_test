//
//  TwitterManager.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import TwitterKit

private let consumerKey = "3Pie3iDRC3SlIX2ZjlLmDDCiP"
private let consumerSecret = "oXRewc0fWAHGoE1oa48H6X2X3fhW87wiL1Quf64j68lmJdvtHK"

class TwitterManager {
  
  // MARK: - Singleton.
  
  private static let sharedInstance = TwitterManager()
  
  private init() {
    twitterInstance = TWTRTwitter.sharedInstance()
    twitterInstance.start(withConsumerKey: consumerKey, consumerSecret: consumerSecret)
    if let userID = TWTRTwitter.sharedInstance().sessionStore.session()?.userID {
      client = TWTRAPIClient(userID: userID)
    } else {
      client = TWTRAPIClient()
    }
  }
  
  @discardableResult
  class func shared() -> TwitterManager {
    return self.sharedInstance
  }
  
  // MARK: Private properties
  
  let twitterInstance: TWTRTwitter
  let client: TWTRAPIClient
  
  // MARK: Public API
  
  func loadMultipleTweets(completion:@escaping (Result<Array<TWTRTweet>>) -> Void) {
    var tweetIDs: [String] = []
    for index in 23123...23153 {
      if index == 23124 || index == 23126 || index == 23131 || index == 23135 || index == 23136 { continue }
      tweetIDs.append(String(index))
    }
    client.loadTweets(withIDs: tweetIDs) { (tweets, error) -> Void in
      if let error = error { completion(.failure(error)); return }
      guard let tweets = tweets else { completion(.failure(Errors.noDataReturned)); return}
      completion(.success(tweets))
    }
  }
  
}
