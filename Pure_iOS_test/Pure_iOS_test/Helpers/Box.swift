//
//  Box.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation

class Box<T> {
  
  typealias Listener = (T) -> Void
  var value: T { didSet { listener?(value) } }
  private var listener: Listener?
  
  init(_ value: T) {
    self.value = value
  }
  
  func bind(listener: Listener?) {
    self.listener = listener
  }
}
