//
//  TWTRTweet+Extensions.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/22/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import TwitterKit

extension TWTRTweet: MainTableViewCellViewModelProtocol {

  var title: String { return author.name }
  var descriptionText: String { return text }
  var avatarUrl: String { return author.profileImageURL }
  
}
