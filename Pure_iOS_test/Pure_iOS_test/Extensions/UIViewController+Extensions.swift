//
//  UIViewController+Extensions.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
  
  func showAlert(title: String, message: String) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
    self.present(alert, animated: true, completion: nil )
  }
  
  func showAlert(title: String, message: String, button: String, handler:@escaping (UIAlertAction) -> Void) {
    let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: button, style: .default, handler: handler))
    self.present(alert, animated: true, completion: nil)
  }
  
}

extension UIViewController {
  
  var activityIndicatorTag: Int { return 999999 }
  
  func startActivityIndicator(style: UIActivityIndicatorViewStyle = .whiteLarge, location: CGPoint? = nil) {
    self.stopActivityIndicator()
    let loc = location ?? view.center
    
    DispatchQueue.main.async {
      let activityIndicator = UIActivityIndicatorView(activityIndicatorStyle: style)
      activityIndicator.tag = self.activityIndicatorTag
      activityIndicator.center = loc
      activityIndicator.hidesWhenStopped = true
      activityIndicator.startAnimating()
      activityIndicator.color = UIColor.red
      self.view.addSubview(activityIndicator)
    }
  }
  
  func stopActivityIndicator() {
    DispatchQueue.main.async {
      if let activityIndicator = self.view.subviews.filter(
        { $0.tag == self.activityIndicatorTag }).first as? UIActivityIndicatorView {
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
      }
    }
  }
  
}
