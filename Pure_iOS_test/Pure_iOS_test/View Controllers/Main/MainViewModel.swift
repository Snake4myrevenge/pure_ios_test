//
//  MainViewModel.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import Foundation
import TwitterKit

typealias ErrorHandler = (MainScreenErrorType) -> Void

enum MainScreenErrorType {
  case twitterError(message: String)
  case githubError(message: String)
}

class MainViewModel {
  
  // MARK: Public properties
  
  var errorHandler: ErrorHandler?
  
  var dataSource: Box<[MainTableViewCellViewModelProtocol]> = Box([MainTableViewCellViewModelProtocol]())
  
  // MARK: Private properties
  
  private let group = DispatchGroup()
  private var tweets: [TWTRTweet] = []
  private var reposInfo: [Item] = []
  
  // MARK: Public API
  
  func retrieveDataSource() {
    group.enter()
    retrieveReposInfo()
    
    group.enter()
    retrieveTweets()
    
    group.notify(queue: .main) {
      var result: [MainTableViewCellViewModelProtocol] = []
      result.append(contentsOf: self.tweets)
      result.append(contentsOf: self.reposInfo)
      self.dataSource.value = result
    }
  }
  
  // MARK: Private API
  
  private func retrieveReposInfo() {
    ApiClient.shared().request(apiMethod: "", responseClass: GithubResponse.self) { result in
      switch result {
      case .success(let githubResponse):
        self.reposInfo = githubResponse.items
      case .failure(let error):
        self.errorHandler?(.githubError(message: error.localizedDescription))
      }
      self.group.leave()
    }
  }
  
  private func retrieveTweets() {
    TwitterManager.shared().loadMultipleTweets { result in
      switch result {
      case .success(let tweets):
        self.tweets = tweets
      case .failure(let error):
        self.errorHandler?(.twitterError(message: error.localizedDescription))
      }
      self.group.leave()
    }
  }
  
}
