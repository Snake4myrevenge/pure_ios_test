//
//  MainTweetTableViewCell.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

class MainTweetTableViewCell: UITableViewCell {
  
  // I should use one class of cell but when I started I didn't want to make similar UI.
  
  @IBOutlet private(set) weak var titleLabel: UILabel!
  @IBOutlet private(set) weak var descriptionLabel: UILabel!
  @IBOutlet private(set) weak var avatarImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    avatarImageView.frame.size.width = 50.0
    avatarImageView.frame.size.height = 50.0
    avatarImageView.layer.masksToBounds = true
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
  }
  
  override func layoutSubviews() {
    super.layoutSubviews()
    avatarImageView.frame.origin.y = (frame.height / 2) - (avatarImageView.frame.height / 2)
    avatarImageView.frame.origin.x = 15.0
    titleLabel.frame.origin.y = 10.0
    titleLabel.frame.origin.x = avatarImageView.frame.maxX + 10.0
    titleLabel.frame.size.width = frame.width - (avatarImageView.frame.maxX + 20.0)
    descriptionLabel.frame = titleLabel.frame
    descriptionLabel.frame.origin.y = titleLabel.frame.maxY + 10.0
    descriptionLabel.frame.size.height = frame.height - (titleLabel.frame.maxY + 20.0)
  }

}

extension MainTweetTableViewCell: MainTableViewCellProtocol {}
