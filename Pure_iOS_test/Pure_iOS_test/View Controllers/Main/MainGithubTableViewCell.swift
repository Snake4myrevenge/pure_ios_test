//
//  MainGithubTableViewCell.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/22/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

class MainGithubTableViewCell: UITableViewCell {
  
  // I should use one class of cell but when I started I didn't want to make similar UI.

  @IBOutlet private(set) weak var titleLabel: UILabel!
  @IBOutlet private(set) weak var descriptionLabel: UILabel!
  @IBOutlet private(set) weak var avatarImageView: UIImageView!
  
  override func awakeFromNib() {
    super.awakeFromNib()
    avatarImageView.frame.size.width = 50.0
    avatarImageView.frame.size.height = 50.0
    avatarImageView.layer.masksToBounds = true
    avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
  }

  override func layoutSubviews() {
    super.layoutSubviews()
    avatarImageView.frame.origin.y = (frame.height / 2) - (avatarImageView.frame.height / 2)
    avatarImageView.frame.origin.x = (frame.maxX - 15.0 - avatarImageView.frame.size.width)
    titleLabel.frame.origin.y = 10.0
    titleLabel.frame.origin.x = 15.0
    titleLabel.frame.size.width = frame.width - (avatarImageView.frame.size.width + 35.0)
    descriptionLabel.frame = titleLabel.frame
    descriptionLabel.frame.origin.y = titleLabel.frame.maxY + 10.0
    descriptionLabel.frame.size.height = frame.height - (titleLabel.frame.maxY + 20.0)
  }
  
}

extension MainGithubTableViewCell: MainTableViewCellProtocol {}
