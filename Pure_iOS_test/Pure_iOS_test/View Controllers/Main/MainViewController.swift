//
//  MainViewController.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit

private let kMainTweetTableViewCellReuseIdentifier = "MainTweetTableViewCell"
private let kMainGithubTableViewCellReuseIdentifier = "MainGithubTableViewCell"

class MainViewController: UIViewController {
  
  // MARK: - Outlets
  
  @IBOutlet private weak var tableView: UITableView!
  
  // MARK: - Public properties
  
  // MARK: - Private properties
  
  private lazy var viewModel = MainViewModel()
  private var dataSource: [MainTableViewCellViewModelProtocol] = [] {
    didSet {
      tableView.reloadData()
      stopActivityIndicator()
      refreshControl.endRefreshing()
    }
  }
  
  private let refreshControl = UIRefreshControl()
  
  // MARK: - View controller's life cycle
  
  override func viewDidLoad() {
    super.viewDidLoad()
    addRefreshControl()
    bindErrorHandler()
    bindDataSource()
    viewModel.retrieveDataSource()
    startActivityIndicator()
  }
  
  override func viewDidLayoutSubviews() {
    super.viewDidLayoutSubviews()
    tableView.frame = view.frame
    tableView.frame.size.height = tableView.frame.size.height - 20.0
    tableView.frame.origin.y = 20.0
  }
  
  // MARK: - Public API
  
  // MARK: - Private API
  
  private func bindErrorHandler() {
    viewModel.errorHandler = { [weak self] error in
      self?.stopActivityIndicator()
      self?.refreshControl.endRefreshing()
      switch error {
      case .twitterError(let message):
        self?.showAlert(title: "Error", message: message)
      default: break // Here we show banner from API client. We can add additional error handling here if needed.
      }
    }
  }
  
  private func bindDataSource() {
    viewModel.dataSource.bind { [weak self] dataSource in
      self?.dataSource = dataSource
    }
  }
  
  private func addRefreshControl() {
    if #available(iOS 10.0, *) {
      tableView.refreshControl = refreshControl
    } else {
      tableView.addSubview(refreshControl)
    }
    refreshControl.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
  }
  
  @objc private func refreshData(_ sender: Any) {
    viewModel.retrieveDataSource()
  }
  
}

extension MainViewController: UITableViewDelegate, UITableViewDataSource {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return dataSource.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let model = dataSource[indexPath.row]
    if model.type == .twitter {
      let cell = tableView.dequeueReusableCell(withIdentifier: kMainTweetTableViewCellReuseIdentifier, for: indexPath) as! MainTweetTableViewCell
      cell.updateCellWithModel(model)
      return cell
    } else {
      let cell = tableView.dequeueReusableCell(withIdentifier: kMainGithubTableViewCellReuseIdentifier, for: indexPath) as! MainGithubTableViewCell
      cell.updateCellWithModel(model)
      return cell
    }
  }
  
}
