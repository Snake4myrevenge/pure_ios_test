//
//  LoginViewController.swift
//  Pure_iOS_test
//
//  Created by Alexander on 7/21/18.
//  Copyright © 2018 Alexander. All rights reserved.
//

import UIKit
import TwitterKit

private let kToMainViewControllerSegueIdentifier = "To main view controller"

class LoginViewController: UIViewController {
  
  // MARK: - View controller's life cycle

  override func viewDidLoad() {
    super.viewDidLoad()
    addTwitterLoginButtonWithHandler()
  }

  // MARK: - Private API
  
  private func addTwitterLoginButtonWithHandler() {
    let logInButton = TWTRLogInButton(logInCompletion: { session, error in
      if (session != nil) {
        self.presentMainViewController()
      } else {
        self.showAlert(title: "OOPS!", message: "Something goes wrong.")
      }
    })
    logInButton.center = view.center
    logInButton.frame.origin.y = logInButton.frame.origin.y + 75.0
    view.addSubview(logInButton)
  }
  
  private func presentMainViewController() {
    startActivityIndicator()
    Timer.scheduledTimer(withTimeInterval: 3.0, repeats: false) { timer in
      self.stopActivityIndicator()
      self.performSegue(withIdentifier: kToMainViewControllerSegueIdentifier, sender: nil)
    }
  }

}

